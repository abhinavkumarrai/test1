#  #

### why load balancer is required ? ##
A load balancer acts as the “traffic cop” sitting in front of your servers and routing client requests across all servers capable of fulfilling those requests in a manner that maximizes speed and capacity utilization and ensures that no one server is overworked, which could degrade performance.

### Solutions Load Balancers Provide- ###

When faced with mounting demand from users, and maxing out the performance of the machine hosting your service, you have two options: scale up or scale out. Scaling up (i.e., vertical scaling) has physical computational limits. Scaling out (i.e., horizontal scaling) allows you to distribute the computational load across as many systems as necessary to handle the workload. When scaling out, a load balancer can help distribute the workload among an array of servers, while also allowing capacity to be added or removed as necessary.

![](download2.png)

## Load Balancer ##

It is a device that acts as a reverse proxy and distributes network or application traffic across a number of servers. Load balancers are used to increase capacity (concurrent users) and reliability of applications. They improve the overall performance of applications by decreasing the burden on servers associated with managing and maintaining application and network sessions, as well as by performing application-specific tasks.

Load balancers are generally grouped into two categories: Layer 4 and Layer 7. Layer 4 load balancers act upon data found in network and transport layer protocols (IP, TCP, FTP, UDP). Layer 7 load balancers distribute requests based upon data found in application layer protocols such as HTTP.

Requests are received by both types of load balancers and they are distributed to a particular server based on a configured algorithm. Some industry standard algorithms are:

* Round Robin
* Weighted round robin
* Least connections
* Least response time



  ![](download.png)

  ___

### Scaling ###
In computer graphics, scaling is a process of modifying or altering the size of objects. Scaling may be used to increase or reduce the size of object. Scaling subjects the coordinate points of the original object to change. Scaling factor determines whether the object size is to be increased or reduced. 



![](download3.png)

Feature scaling is essential for machine learning algorithms that calculate distances between data.Therefore, the range of all features should be normalized so that each feature contributes approximately proportionately to the final distance.

## Scaling can be categorised into 2 types: ##

 Vertical Scaling : When new resources are added in the existing system to meet the expectation, it is known as vertical scaling.
Consider a rack of servers and resources that comprises of the existing system. Now when the existing system fails to meet the expected needs, and the expected needs can be met by just adding resources, this is considered as vertical scaling.


Horizontal Scaling: When new server racks are added in the existing system to meet the higher expectation, it is known as Horizontal Scaling.
Consider a rack of servers and resources that comprises of the existing system. Now when the existing system fails to meet the expected needs, and the expected needs cannot be met by just adding resources, we need to add completely new servers. This is considered as horizontal scaling.
Horizontal scaling is difficult and also costlier than Vertical Scaling. It also requires more time to be fixed.


![](download4.png)

____
* Reference Links:

  [Google](http://google.com)

  [Load Balancing](https://www.nginx.com/resources/glossary/load-balancing/#)

___






  

  



